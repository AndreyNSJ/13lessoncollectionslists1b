package com.company;

import phonebook.Phonebook;

public class Main {

    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        phonebook.add("Andrey", "+3809678965421");
        phonebook.add("Oleg", "+3809690965423");
        phonebook.add("Olga", "+3809877965322");
        phonebook.add("Andrey", "+3804875698749");
        System.out.println("Andrey(find): ");
        System.out.println(phonebook.find("Andrey").getPhoneNumber());
        System.out.println("Andrey(findAll): ");
        System.out.println(phonebook.find("Andrey").getPhoneNumber());

    }
}
